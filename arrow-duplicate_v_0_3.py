# Arrow Duplicate v 0.3 by mbbmbbmm
# Ich brauche eine bool property fuer shift alt arrow -> linked copy
# bzw. alt arrow -> linked copy, shift alt arrow -> normal copy
# oder doch nen button... waere eigentlich praktischer
# TODO doesn't work with rotated objects -> child objects are offset
# TODO probably get the aabb of all selected objects and use that
# TODO would also give the opportunity to multi-copy with correct bounds

bl_info = {
    "name": "Arrow Duplicate",
    "author": "mbbmbbmm",
    "version": (0, 3, 0),
    "blender": (2, 92, 0),
    "location": "View3D > Object Mode",
    "description": "",
    "warning": "",
    "doc_url": "",
    "category": "",
}

import bpy
import mathutils
import math


#-------------Operators----------------

class OBJECT_OT_arrow_duplicate_right(bpy.types.Operator):
    bl_idname = "op.arrow_duplicate_right"
    bl_label = "Arrow Duplicate to right direction"
    bl_description = "Duplicate and move by bounds in positive X direction (global only)"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    def execute(self, context):
        dir = mathutils.Vector((1.0, 0.0, 0.0))
        DuplicateObj(context, dir)
        return {'FINISHED'}


class OBJECT_OT_arrow_duplicate_fwd(bpy.types.Operator):
    bl_idname = "op.arrow_duplicate_fwd"
    bl_label = "Arrow Duplicate to forward direction"
    bl_description = "Duplicate and move by bounds in positive Y direction (global only)"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    def execute(self, context):
        dir = mathutils.Vector((0.0, 1.0, 0.0))
        DuplicateObj(context, dir)
        return {'FINISHED'}

class OBJECT_OT_arrow_duplicate_left(bpy.types.Operator):
    bl_idname = "op.arrow_duplicate_left"
    bl_label = "Arrow Duplicate to left direction"
    bl_description = "Duplicate and move by bounds in negative X direction (global only)"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    def execute(self, context):
        dir = mathutils.Vector((-1.0, 0.0, 0.0))
        DuplicateObj(context, dir)
        return {'FINISHED'}

class OBJECT_OT_arrow_duplicate_back(bpy.types.Operator):
    bl_idname = "op.arrow_duplicate_back"
    bl_label = "Arrow Duplicate to back direction"
    bl_description = "Duplicate and move by bounds in negative Y direction (global only)"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    def execute(self, context):
        dir = mathutils.Vector((0.0, -1.0, 0.0))
        DuplicateObj(context, dir)
        return {'FINISHED'}


#---------------Helpers----------------

def DuplicateObj(context, dir):
    active_obj = context.view_layer.objects.active
    sel_objs = [obj for obj in bpy.context.selected_objects if obj.type == 'MESH']
    bpy.ops.object.select_all(action='DESELECT')

    dups = []
    active_dup = None
    while len(sel_objs) >= 1:
        obj = sel_objs.pop()

        obj.select_set(True)
        context.view_layer.objects.active = obj
        bounds = obj.dimensions
        # assume we want the child objects to be linked copies as well, even if they are colliders
        # duplicate active object
        bpy.ops.object.duplicate(linked=True, mode='DUMMY')
        dup = context.view_layer.objects.active
        if obj == active_obj:
            active_dup = dup
        rotated_dir = GetRotatedDir(context, dir, bounds)
        dup.location += rotated_dir
        dups.append(dup)
        # get children
        for child in obj.children:
            bpy.ops.object.select_all(action='DESELECT')
            child.select_set(True)
            context.view_layer.objects.active = child
            # duplicate
            bpy.ops.object.duplicate(linked=True, mode='DUMMY')
            # add to location with the same dir
            # i think here is the bug regarding childs with rotated parent
            child.location += rotated_dir
            bpy.ops.object.select_all(action='DESELECT')
            child.select_set(True)
            dup.select_set(True)
            context.view_layer.objects.active = dup
            bpy.ops.object.parent_set(type='OBJECT', keep_transform=True)

        bpy.ops.object.select_all(action='DESELECT')
#            dup.select_set(True)
#            context.view_layer.objects.active = dup
    
    for d in dups:
        d.select_set(True)

    context.view_layer.objects.active = active_dup

def GetRotatedDir(context, dir, bounds):
    view = context.space_data
    view_eul = view.region_3d.view_rotation.to_euler()
    rotated_dir = dir.copy()
    b = bounds.copy()
    #special case: top views
    if view.region_3d.is_orthographic_side_view and view_eul.x < 0.001:
        rotated_dir.rotate(view_eul)
        rotated_scaled_dir =rotated_dir * bounds
        return rotated_scaled_dir
    #normal case: other views
    wrld_down = mathutils.Vector((0.0, 0.0, -1.0))
    cam_fwd = wrld_down.copy()
    cam_fwd.rotate(view_eul)
    cam_fwd = mathutils.Vector((cam_fwd.x, cam_fwd.y, 0.0))
    cam_fwd.normalize()
    wrld_fwd = mathutils.Vector((0.0, 1.0, 0.0))
    dot_fwd = cam_fwd.dot(wrld_fwd)
    degree = 0.0
    if dot_fwd <= -0.707:
        # rotate by 180
        degree = math.radians(180)
    elif dot_fwd > -0.707 and dot_fwd < 0.707:
        wrld_right = mathutils.Vector((1.0, 0.0, 0.0))
        dot_right = cam_fwd.dot(wrld_right)
        if dot_right < -0.707:
            #rotate 90 deg to left
            degree = math.radians(90)
        else:
            #rotate 90 deg to right
            degree = math.radians(-90)
    eul = mathutils.Euler((0.0, 0.0, degree), 'XYZ')
    rotated_dir.rotate(eul)
    rotated_scaled_dir = rotated_dir * b

    return rotated_scaled_dir


#----------------UI--------------------

def ShowMessageBox(message = "", title = "Message", icon = 'ERROR'):
    
    def draw(self, context):
        self.layout.label(text = message)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)


#-------------Register-----------------

classes = [
    OBJECT_OT_arrow_duplicate_right, 
    OBJECT_OT_arrow_duplicate_fwd,
    OBJECT_OT_arrow_duplicate_left, 
    OBJECT_OT_arrow_duplicate_back
]

addon_keymaps = []

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc:
        km = wm.keyconfigs.addon.keymaps.new(name='3D View', space_type='VIEW_3D')

        kmi = km.keymap_items.new(
            OBJECT_OT_arrow_duplicate_right.bl_idname, 
            type='RIGHT_ARROW',
            value='PRESS',
            alt=True
        )
        kmi = km.keymap_items.new(
            OBJECT_OT_arrow_duplicate_fwd.bl_idname, 
            type='UP_ARROW',
            value='PRESS',
            alt=True
        )
        kmi = km.keymap_items.new(
            OBJECT_OT_arrow_duplicate_left.bl_idname, 
            type='LEFT_ARROW',
            value='PRESS',
            alt=True
        )
        kmi = km.keymap_items.new(
            OBJECT_OT_arrow_duplicate_back.bl_idname, 
            type='DOWN_ARROW',
            value='PRESS',
            alt=True
        )
        addon_keymaps.append(km)
    print("ok")

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

    wm = bpy.context.window_manager
    for km in addon_keymaps:
        wm.keyconfigs.addon.keymaps.remove(km)
    addon_keymaps.clear()
